<?php

declare(strict_types=1);

namespace App;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Ramsey\Uuid\Uuid;

class TransactionIdGenerateMiddleware implements MiddlewareInterface
{
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler) : ResponseInterface
    {
        if(!$request->hasHeader('X-TransactionId')){
            $transactionId = Uuid::uuid4()->toString();
            $request = $request->withAddedHeader('X-TransactionId', $transactionId);
        }
        return $handler->handle($request);
    }
}
