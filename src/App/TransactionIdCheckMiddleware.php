<?php

declare(strict_types=1);

namespace App;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

class TransactionIdCheckMiddleware implements MiddlewareInterface
{
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $response = $handler->handle($request);
        $transactionId = $response->hasHeader('X-TransactionId');
        if (!$transactionId) {
            $response = $response->withStatus(500, 'X-TransactionId Result NotFound');
        }
        return $response;
    }
}
