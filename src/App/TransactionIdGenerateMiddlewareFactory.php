<?php

declare(strict_types=1);

namespace App;

use Psr\Container\ContainerInterface;

class TransactionIdGenerateMiddlewareFactory
{
    public function __invoke(ContainerInterface $container) : TransactionIdGenerateMiddleware
    {
        return new TransactionIdGenerateMiddleware();
    }
}
