<?php

declare(strict_types=1);

namespace App;

use Psr\Container\ContainerInterface;

class TransactionIdCheckMiddlewareFactory
{
    public function __invoke(ContainerInterface $container) : TransactionIdCheckMiddleware
    {
        return new TransactionIdCheckMiddleware();
    }
}
