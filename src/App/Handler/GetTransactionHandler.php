<?php

declare(strict_types=1);

namespace App\Handler;

use Laminas\Diactoros\Response\JsonResponse;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

class GetTransactionHandler implements RequestHandlerInterface
{
    public function handle(ServerRequestInterface $request) : ResponseInterface
    {
        $transactionId = $request->getHeader('X-TransactionId');
        $response = new JsonResponse(['status' => true]);
        return $response->withAddedHeader('X-TransactionId', $transactionId);
    }
}
