<?php

declare(strict_types=1);

namespace App\Handler;

use Psr\Container\ContainerInterface;

class GetTransationHandlerFactory
{
    public function __invoke(ContainerInterface $container) : GetTransactionHandler
    {
        return new GetTransactionHandler();
    }
}
